/* kbd.c */

#include "kbd.h"
#include "kbuf.h"
#include "rb.h"

#include "config.h"

#ifndef CONFIG_KBD_KEYBUFSIZE
#error "CONFIG_KBD_KEYBUFSIZE is not defined!"
#endif

#ifdef CONFIG_DEBUG_KBD
#include "syslog.h"
#include <stdlib.h> /* for itoa() */
#define kbdbg syslog
#else
#define kbdbg (void)
#endif

struct kbd_llops_s {
	void (*fp_scan)(void);
	uint8_t (*fp_changed)(void);
};

struct kbd_s {
	struct kbd_llops_s op;
	uint8_t changed;
	struct rb_s *keybuf;
};

static void dequeue_key(const struct kbd_s *k, uint8_t *key);

static char s_kbd_kbuf[CONFIG_KBD_KEYBUFSIZE];

static struct rb_s s_kbd_keybuf = {
	.buf		= s_kbd_kbuf,
	.len		= CONFIG_KBD_KEYBUFSIZE,
};

static struct kbd_s s_kbd = {
	.keybuf		= &s_kbd_keybuf,
};

static void ll_scan(const struct kbd_s *k)
{
	(*k->op.fp_scan)();
}

static uint8_t ll_changed(const struct kbd_s *k)
{
	return (*k->op.fp_changed)();
}

static void dequeue_key(const struct kbd_s *k, uint8_t *key)
{
	struct rb_s *b = k->keybuf;

	if (rb_empty(b)) {
		*key = 0;
		return;
	}
	*key = rb_get(b);
}

void kbd_init(void (*fp_scan)(void), uint8_t (*fp_changed)(void))
{
	s_kbd.changed = 0;
	s_kbd.op.fp_scan = fp_scan;
	s_kbd.op.fp_changed = fp_changed;

	/* Reset the key buffer */
	rb_init(s_kbd.keybuf);

	kbuf_init();
}

void kbd_update(void)
{
	ll_scan(&s_kbd);
	s_kbd.changed = ll_changed(&s_kbd);
}

uint8_t kbd_changed(void)
{
	return s_kbd.changed;
}

void kbd_getkey(uint8_t *key)
{
	dequeue_key(&s_kbd, key);
	s_kbd.changed--;
}

/* NOTE: Only to be used by lower-level modules */
void kbd_enqueue_key(const uint8_t key)
{
	struct rb_s *b = &s_kbd_keybuf;

	if (rb_full(b))
		return;
	rb_put(b, key);
}
