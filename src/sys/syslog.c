/* syslog.c */

#include "syslog.h"

static void (*s_putc_handler)(char s);

void syslog_init(void (*putc)(const char s))
{
	s_putc_handler = putc;
}

void syslog(char *s)
{
	while (*s)
		(*s_putc_handler)(*s++);
}
