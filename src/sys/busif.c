/* busif.c */

#include "busif.h"

#include "config.h"

#ifdef CONFIG_DEBUG_BUSIF
#include "syslog.h"
#define bidbg	syslog
#else
#define bidbg	(void)
#endif

#ifndef CONFIG_BUSIF_TXBUFSIZE
#error "CONFIG_BUSIF_TXBUFSIZE is not defined!"
#endif

#include "rb.h"

struct busif_s {
	bool master;
	struct busif_io_s *io;
	struct pin_s *pin_intout;
	struct rb_s *txbuf;
};

static uint8_t s_busiftxb[CONFIG_BUSIF_TXBUFSIZE];

static struct rb_s s_busiftxbuf = {
	.buf	= s_busiftxb,
	.len	= CONFIG_BUSIF_TXBUFSIZE,
};
static struct busif_s s_busif;

static void llupdate(void);
static bool llrxready(void);
static uint8_t llread(void);
static bool lltxready(void);
static void llwrite(const uint8_t b);
static uint8_t bi_rx(void);
static void bi_tx(const uint8_t b);

static void llupdate(void)
{
	struct busif_s *bi = &s_busif;

	(bi->io->update)();
}

static bool llrxready(void)
{
	struct busif_s *bi = &s_busif;

	return (bi->io->rxready)();
}

static uint8_t llread(void)
{
	struct busif_s *bi = &s_busif;

	return (bi->io->read)();
}

static bool lltxready(void)
{
	struct busif_s *bi = &s_busif;

	return (bi->io->txready)();
}

static void llwrite(const uint8_t b)
{
	struct busif_s *bi = &s_busif;

	(bi->io->write)(b);
}

static uint8_t bi_rx(void)
{
	if (!llrxready())
		return 0;

	return llread();
}

static void bi_tx(const uint8_t b)
{
	llwrite(b);
}

void busif_init(struct busif_io_s *io, struct pin_s *pin_intout, bool master)
{
	struct busif_s *bi = &s_busif;

	bi->master = master;
	bi->io = io;
	bi->pin_intout = pin_intout;

	rb_init(bi->txbuf);

	if (master) {
		/* Initialize interrupt input pin (input, no pull-up, no pull-down) */
		gpio_setpinconfig(pin_intout, false, false, false);
		/* TODO Enable hardware interrupt pin */
	} else {
		/* Initialize interrupt output pin (output, no pull-up, no pull-down) */
		gpio_setpinconfig(pin_intout, true, false, false);
		gpio_writepin(pin_intout, true); /* set output high by default */
	}
}

void busif_update(void)
{
	struct busif_s *bi = &s_busif;
	uint8_t b;

	bidbg("b u");
	if (bi->master) {
		/* Return if not notified by slave (interrupt input is high/idle) */
		if (gpio_readpin(bi->pin_intout)) {
			bidbg("\r\n");
			return;
		}
		bidbg(" N\r\n");

		bi_tx(0x55); /* Send test byte */
#if 0
		/* TODO Select the slave */
		/* TODO Start communication with slave */
		/* TODO Read keys changed from slave (ugly, not here, I know) */

		/* TODO SEND read status register command */
		bi_tx(0x01); /* 0000 0001 */
		/* TODO READ byte */
		bi_rx();


		/* TODO Decode status register value */


		/* TODO SEND read keys command */
		bi_tx(0x11); /* 0001 0001 */
		/* TODO READ keys */
		bi_rx();
#endif
	} else {
		/* TODO Get slave mode recognition of "beeing addressed" working */
#if 0
		bidbg("\r\n");
		llupdate();
#else
		/* FIXME Is never true?! */
		if (llrxready()) {
			bidbg(" A\r\n");

			b = llread();
		}
		bidbg("\r\n");
#endif

		/* TODO Check if we have been addressed */
		/* TODO Dispatch request */
		/* Check if there is something to transmit */
		if (rb_empty(bi->txbuf))
			return;
		/* If there is something to transmit, notify master
		 * by making interrupt output low. */
	//	gpio_writepin(bi->pin_intout, false);
		/* TODO Note that we notified master and are waiting to
		 * be called (in order to transmit the enqueued data */
	}
}

void busif_put(const uint8_t b)
{
	struct busif_s *bi = &s_busif;

	if (bi->master) {
		/* TBD */
	} else {
		/* FIXME This is only temporary as long as the bug below (txbuf
		 * is always full) gets fixed. */
		gpio_writepin(bi->pin_intout, false);

		/* TODO FIXME This seems to be true always or at least
		 * is the buffer filled when we test it (maybe the rotary
		 * encoders fill it on initialization, because they are active
		 * low, but the 'last' buf is initialized with zero, hence
		 * on initialization, all 8 knobs generate change events and
		 * the buffer is full? */
		if (rb_full(bi->txbuf))
			return;

		rb_put(bi->txbuf, b);
	}
}
