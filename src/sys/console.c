/* console.c */

#include "console.h"

#include "config.h"

#ifndef CONFIG_CONSOLE_TXBUFSIZE
#error "CONFIG_CONSOLE_TXBUFSIZE must be defined"
#endif

#ifndef CONFIG_CONSOLE_RXBUFSIZE
#error "CONFIG_CONSOLE_RXBUFSIZE must be defined"
#endif

static void ll_putc(const char c);
static char ll_getc(void);
static void txupdate(void);
static void rxupdate(void);

static void (*g_console_fp_putc)(const char);
static char (*g_console_fp_getc)(void);

static char g_console_txb[CONFIG_CONSOLE_TXBUFSIZE];
static char g_console_rxb[CONFIG_CONSOLE_RXBUFSIZE];

#include "rb.h"

static struct rb_s g_console_txbuf = {
	.buf = g_console_txb,
	.len = CONFIG_CONSOLE_TXBUFSIZE,
};

static struct rb_s g_console_rxbuf = {
	.buf = g_console_rxb,
	.len = CONFIG_CONSOLE_RXBUFSIZE,
};

static void ll_putc(const char c)
{
	(*g_console_fp_putc)(c);
}

static char ll_getc(void)
{
	return (*g_console_fp_getc)();
}

static void txupdate(void)
{
	struct rb_s *b = &g_console_txbuf;
	char c;

	/* Check if there is something in the tx buffer to transmit */

	if (!rb_empty(b)) {
		c = rb_get(b);
		ll_putc(c);
	} else
		rb_flush(b);
}

/* TODO IIRC The RX part of the console is not working yet */
static void rxupdate(void)
{
	struct rb_s *b = &g_console_rxbuf;
	char c;

	/* Check if there way a byte received by the hardware */

#include "uart.h"

	/* TODO Get rid of direct access to uart_received() and use
	 * function pointer that is passed to init func. */
	if (!uart_received())
		return;
	c = ll_getc();
	rb_put(b, c);
}

void console_init(void (*fp_putc)(const char), char (*fp_getc)(void))
{
	g_console_fp_putc = fp_putc;
	rb_init(&g_console_txbuf);

	g_console_fp_getc = fp_getc;
	rb_init(&g_console_rxbuf);
}

void console_putc(const char c)
{
	struct rb_s *b = &g_console_txbuf;

	/* Put the char into the tx buffer */
	rb_put(b, c);
}

char console_getc(void)
{
	return -1;
}

void console_update(void)
{
	txupdate();
	rxupdate();
}
