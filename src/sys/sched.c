/* sched.c */

#include "sched.h"

#include "config.h"

#ifndef CONFIG_SCHED_TASKS_MAX
#error "CONFIG_SCHED_TASKS_MAX must be defined"
#endif

#include <stdbool.h>


struct task_s
{
	void (*fp)(void);	/**< Pointer to main function of task. */

	uint16_t delay;		/**< Number of ticks left until next execution. */
	uint16_t periode;	/**< Number of ticks between periodic execution of task. */
	bool run;		/**< Task is ready to be executed. */
};

static struct task_s s_tasklist[CONFIG_SCHED_TASKS_MAX];

void sched_init(void)
{
	const int max = CONFIG_SCHED_TASKS_MAX;
	int i;
	struct task_s *t;

	/* Clear the task list */
	for (i = 0; i < max; i++)
	{
		t = &s_tasklist[i];
		t->fp		= 0,
		t->delay	= 0;
		t->periode	= 0;
	}
}

int sched_addtask(void (*fp)(void), const uint16_t delay, const uint16_t periode)
{
	const int max = CONFIG_SCHED_TASKS_MAX;
	int i = 0;

	/* Find a free slot for the task in the task list,
	 * otherwise return with error */
	while (s_tasklist[i].fp != 0 && i < max)
		i++;

	if (i == max)
		return -1;

	struct task_s *t = &s_tasklist[i];

	t->fp		= fp;
	t->delay	= delay;
	t->periode	= periode;
	t->run		= false;

	return i;
}

void sched_start(void)
{
	return;
}

void sched_update(void)
{
	const int max = CONFIG_SCHED_TASKS_MAX;
	int i;
	struct task_s *t;

	/* Go over the task list and check if a task is scheduled for execution */
	for (i = 0; i < max; i++)
	{
		t = &s_tasklist[i];

		if (t->fp == 0)
			continue;

		if (t->delay == 0)
		{
			t->delay = t->periode;
			t->run = true;
		}
		else
		{
			t->delay--;
		}
	}
}

void sched_dispatch(void)
{
	const int max = CONFIG_SCHED_TASKS_MAX;
	int i;
	struct task_s *t;

	for (i = 0; i < max; i++)
	{
		t = &s_tasklist[i];
		if (t->run != true)
			continue;

		/* Execute the task that is marked to be executed */
		(*t->fp)();
		t->run = false;

		/* When task has no periode, it was scheduled to be run
		 * once only, so we delete it from the task list. */
		if (t->periode == 0)
			t->fp = 0;
	}
}
