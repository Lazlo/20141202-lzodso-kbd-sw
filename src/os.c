/* os.c */

#include "os.h"

#include "config.h"

#include "hw/irq.h"
#include "hw/pwr.h"
#include "hw/timer1.h"
#include "hw/uart.h"
#ifdef CONFIG_SIMULAVR
#include "hw/vuart.h"
#endif

#include "sys/console.h"
#include "sys/syslog.h"
#include "sys/sched.h"
#include "sys/uptime.h"

#ifndef FW_BANNER
#error "FW_BANNER is not defined!"
#endif
#ifndef FW_VERSION
#error "FW_VERSION is not defined!"
#endif
#ifndef CONFIG_UARTBAUD
#error "CONFIG_UARTBAUD is not defined!"
#endif

static void fwbanner(void);
static void systick_handler(void);
static void initstdio(void);
static void addostasks(void);

static void fwbanner(void)
{
	syslog(FW_BANNER);
	syslog(" ");
	syslog(FW_VERSION);
	syslog("\r\n");
}

static void systick_handler(void)
{
	sched_update();
}

#ifndef CONFIG_NO_LOGREADY
#define log_ready(s)	syslog(s " ready\r\n")
#else
#define log_ready(s)
#endif

static void initstdio(void)
{
	void (*fp_llputc)(const char);
	char (*fp_llgetc)(void);

#ifndef CONFIG_SIMULAVR
	fp_llputc = uart_putc;
	fp_llgetc = uart_getc;
#else
	fp_llputc = vuart_putc;
	fp_llgetc = vuart_getc;
#endif

#ifndef CONFIG_SIMULAVR
	/* Initialize U(S)ART hardware */
	uart_init(CONFIG_UARTBAUD);
#endif
	/* Setup console and logging facilities */
	console_init(fp_llputc, fp_llgetc);
	syslog_init(console_putc);

	fwbanner();
	log_ready("uart");
}

static void addostasks(void)
{
	sched_addtask(console_update,	0,    2);
#ifndef CONFIG_NO_UPTIME
	sched_addtask(uptime_show,	0, 1000);
#endif
}

void os_init(void)
{
	/* Setup standard IO */
	initstdio();

	/* Setup the hardware timer for use with the scheduler */
	timer1_init();
	timer1_sethandler(systick_handler);
	log_ready("timer1");

	/* Initialize the scheduler */
	sched_init();
	log_ready("sched");
}

void os_start(void)
{
	addostasks();

	sched_start();
	irq_enable(true);
	syslog("irqs enabled\r\n");

	pwr_sleep_enable(true);
	while (1) {
		sched_dispatch();
		pwr_sleep();
	}
}
