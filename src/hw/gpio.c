/* gpio.c */

#include "gpio.h"

void gpio_create(pin_t *pin, const uint8_t nr,
			volatile uint8_t *reg_dir,
			volatile uint8_t *reg_in,
			volatile uint8_t *reg_out)
{
	pin->nr		= nr;
	pin->reg_dir	= reg_dir;
	pin->reg_in	= reg_in;
	pin->reg_out	= reg_out;
	return;
}

void gpio_getpinconfig(pin_t *pin)
{
	/* TODO Get pin direction */
	/* TODO Get pin pullup/pulldown configuration */
	return;
}

void gpio_setpinconfig(pin_t *pin, bool output, bool pullup, bool pulldown)
{
	/* Set pin direction */
	if (output)
		*pin->reg_dir |= (1 << pin->nr);
	else
		*pin->reg_dir &= ~(1 << pin->nr);

	/* TODO Set pullup/pulldown configuration */
}

void gpio_writepin(pin_t *pin, bool on)
{
	/* Set bit in pin output register */
	if (on)	*pin->reg_out |= (1 << pin->nr);
	else	*pin->reg_out &= ~(1 << pin->nr);
}

bool gpio_readpin(pin_t *pin)
{
	bool on;
	/* Get bit from pin input register */
	on = *pin->reg_in & (1 << pin->nr) ? true : false;
	return on;
}
