/* spi.c */

#include "spi.h"

#include "config.h"

#include <avr/io.h>

struct spi_regs_s {
	volatile uint8_t *cr;	/**< Memory address of SPI control register. */
	volatile uint8_t *sr;	/**< Memory address of SPI status register. */
	volatile uint8_t *dr;	/**< Memory address of SPI data register. */
};

struct spi_dev_s {
	struct spi_regs_s *r;		/**< Pointer to set of SPI register addresses. */
	const struct spi_cfg_s *cfg;	/**< SPI configuration. */
	const struct spi_pins_s *pin;	/**< SPI pins. */
};

static void spi_setmode(const struct spi_dev_s *dev, const bool master);
static void spi_setsckmode(const struct spi_dev_s *dev, const uint8_t mode);
static void spi_settxorder(const struct spi_dev_s *dev, const bool lsbfirst);
static void spi_setsckrate(const struct spi_dev_s *dev, const uint8_t rate);
static void spi_turnon(const struct spi_dev_s *dev, const bool on);
static uint8_t spi_status(const struct spi_dev_s *dev);
static bool spi_trxready(const struct spi_dev_s *dev);
static uint8_t spi_receive(const struct spi_dev_s *dev);
static void spi_transmit(const struct spi_dev_s *dev, const uint8_t b);

#ifdef CONFIG_DEBUG_SPI
#include <syslog.h>
#define spdbg syslog
#else
#define spdbg (void)
#endif

static struct spi_regs_s g_spiregs = {
	.cr	= &SPCR,
	.sr	= &SPSR,
	.dr	= &SPDR,
};

static struct spi_dev_s g_spidev = {
	.r	= &g_spiregs,
	.cfg	= 0,		/* initialize with 0, is set by spi_init() */
	.pin	= 0,		/* initialize with 0, is set by spi_init() */
};

/* Set true for master, false for slave operation */
static void spi_setmode(const struct spi_dev_s *dev, const bool master)
{
	volatile uint8_t *reg;
	const uint8_t mask = (1 << MSTR);

	reg = dev->r->cr;
	if (master)
		*reg |= mask;
	else
		*reg &= ~mask;

	spdbg("spi: mode ");
	spdbg(master ? "m" : "s");
	spdbg("\r\n");
}

/* NOTE SCK mode needs to be same on master and slave. */
static void spi_setsckmode(const struct spi_dev_s *dev, const uint8_t mode)
{
	volatile uint8_t *reg;
	const uint8_t mask = (1 << CPOL)|(1 << CPHA);
	char modestr[2] = { [1] = '\0' };

	reg = dev->r->cr;
	*reg &= ~mask;
	*reg |= mode & mask;

	modestr[0] = '0' + mode;
	spdbg("spi: sckmode ");
	spdbg(modestr);
	spdbg("\r\n");
}

/* NOTE TX order needs to be same on master and slave. */
static void spi_settxorder(const struct spi_dev_s *dev, const bool lsbfirst)
{
	volatile uint8_t *reg;
	const uint8_t mask = (1 << DORD);

	reg = dev->r->cr;
	if (lsbfirst)
		*reg |= mask;
	else
		*reg &= mask;

	spdbg("spi: txorder ");
	spdbg(lsbfirst ? "L" : "M");
	spdbg("SB\r\n");
}

/* Configure bus clock speed
 *
 * NOTE: Only for master operation
 *
 * Values for rate argument on ATmega8:
 * 0 /4
 * 1 /16
 * 2 /64
 * 3 /128
 * 4 /2
 * 5 /8
 * 6 /32
 * 7 /64
 */
static void spi_setsckrate(const struct spi_dev_s *dev, const uint8_t rate)
{
	volatile uint8_t *pr_reg;
	volatile uint8_t *x2_reg;
	const uint8_t pr_mask = (1 << SPR1)|(1 << SPR0);
	const uint8_t x2_mask = (1 << SPI2X);
	char ratestr[2] = { [1] = '\0' };

	pr_reg = dev->r->cr;
	x2_reg = dev->r->sr;

	*pr_reg &= ~pr_mask;
	*pr_reg |= rate & pr_mask;

	if (rate & (1 << 2)) /* 3rd bit in rate value stants for SPI2X */
		*x2_reg |= x2_mask;
	else
		*x2_reg &= ~x2_mask;

	ratestr[0] = '0' + rate;
	spdbg("spi: sckrate ");
	spdbg(ratestr);
	spdbg("\r\n");
}

static void spi_turnon(const struct spi_dev_s *dev, const bool on)
{
	volatile uint8_t *reg;
	const uint8_t mask = (1 << SPE);

	reg = dev->r->cr;
	if (on)
		*reg |= mask;
	else
		*reg &= ~mask;

	spdbg("spi: pwr ");
	spdbg(on ? "on" : "off");
	spdbg("\r\n");
}

#include "strutils.h"

static uint8_t spi_status(const struct spi_dev_s *dev)
{
	volatile uint8_t *reg;
	uint8_t status;
	char istr[4] = { [3] = '\0' };

	reg = dev->r->sr;
	status = *reg;

	itoa(status, istr);
	spdbg("s s ");
	spdbg(istr);
	spdbg("\r\n");

	return status;
}

static bool spi_trxready(const struct spi_dev_s *dev)
{
	const uint8_t mask = (1 << SPIF);

	/* NOTE: This is the only place the status register is read
	 * If this register would also be read by another function
	 * we would no longer be sure to read the correct state, as
	 * some bits of the register might get cleared by reading the
	 * register */
	return spi_status(dev) & mask ? true : false;
}

static uint8_t spi_receive(const struct spi_dev_s *dev)
{
	volatile uint8_t *reg;
	uint8_t b;
	char istr[4] = { [3] = '\0' };

	reg = dev->r->dr;
	b = *reg;

	itoa(b, istr);
	spdbg("s rx ");
	spdbg(istr);
	spdbg("\r\n");

	return b;
}

static void spi_transmit(const struct spi_dev_s *dev, const uint8_t b)
{
	volatile uint8_t *reg;
	char istr[4] = { [3] = '\0' };

	reg = dev->r->dr;
	*reg = b;

	itoa(b, istr);
	spdbg("s tx ");
	spdbg(istr);
	spdbg("\r\n");
}

void spi_init(const struct spi_cfg_s *c, const struct spi_pins_s *pins)
{
	struct spi_dev_s *dev = &g_spidev;

	dev->cfg = c;
	dev->pin = pins;

	if (c->master) {
		gpio_setpinconfig(pins->sck, true, false, false);
		gpio_setpinconfig(pins->mosi, true, false, false);
		gpio_setpinconfig(pins->ss, true, false, false);
		gpio_writepin(pins->ss, true); /* default high/idle */
	} else {
		gpio_setpinconfig(pins->miso, true, false, false);
	}

	spi_setmode(dev, c->master);
	spi_setsckmode(dev, c->sckmode);
	spi_settxorder(dev, c->lsbfirst);
	if (c->master) {
		spi_setsckrate(dev, c->sckrate);
	}

	spi_turnon(dev, true);

	/* TODO Clear SPIF interrupt flag by reading the SPSR and then the SPDR */
	spi_trxready(dev);
	spi_receive(dev);
}

/* NOTE: This function needs to be called periodically. */
void spi_update(void)
{
	struct spi_dev_s *dev = &g_spidev;
	uint8_t b;

	spdbg("s u\r\n");

	/* Check if we received a byte */
	if (spi_trxready(dev)) {
		b = spi_receive(dev);
	}
}

bool spi_rxready(void)
{
	return spi_trxready(&g_spidev);
}

uint8_t spi_read(void)
{
	return spi_receive(&g_spidev);
}

bool spi_txready(void)
{
	return spi_trxready(&g_spidev);
}

void spi_write(const uint8_t b)
{
	struct spi_dev_s *dev = &g_spidev;

	if (dev->cfg->master) {
		gpio_writepin(dev->pin->ss, false);
	}

	spi_transmit(dev, b);

	while (!spi_trxready(dev))
		;

	if (dev->cfg->master) {
		gpio_writepin(dev->pin->ss, true);
	}
}
