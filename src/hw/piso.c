/* piso.c */

#include "piso.h"

static struct pin_s *g_pin_datain;
static struct pin_s *g_pin_clock;
static struct pin_s *g_pin_latch;

static void ndelay(void)
{
}

void piso_init(struct pin_s *pin_datain,
		struct pin_s *pin_clock,
		struct pin_s *pin_latch)
{
	pin_t *p;

	g_pin_datain = pin_datain;
	g_pin_clock = pin_clock;
	g_pin_latch = pin_latch;

	/* PISO DATAIN */

	p = pin_datain;
	gpio_setpinconfig(p, false, false, false);	// input, no pullup, no pulldown

	/* PISO CLOCK */

	p = pin_clock;
	gpio_setpinconfig(p, true, false, false);	// output, no pullup, no pulldown
	gpio_writepin(p, true);				// set output high

	/* PISO LATCH */

	p = pin_latch;
	gpio_setpinconfig(p, true, false, false);	// output, no pullup, no pulldown
	gpio_writepin(p, true);				// set output high
}

void piso_latch(void)
{
	struct pin_s *p = g_pin_latch;
	uint8_t mask	= (1 << p->nr);

	// Pull latch line low, then release it

	*p->reg_out &= ~mask;
	ndelay();
	*p->reg_out |= mask;
}

void piso_clock(void)
{
	struct pin_s *p = g_pin_clock;
	uint8_t mask	= (1 << p->nr);

	// advance clock one periode (pull pin low, then high again)

	*p->reg_out &= ~mask;
	ndelay();
	*p->reg_out |= mask;
}

bool piso_read(void)
{
	struct pin_s *p = g_pin_datain;
	uint8_t mask	= (1 << p->nr);
	bool bit	= false;

	// Read datain pin and advance clock

	bit = *p->reg_in & mask;
	piso_clock();

	return bit;
}

uint8_t piso_readbyte(void)
{
	uint8_t k;
	uint8_t byte = 0;

	// Read a byte from the PISO shift register, starting with the
	// bit on the left (MSB).

	for (k = 8; k > 0; k--)
		if (piso_read())
			byte |= (1 << (k - 1));

	return byte;
}
