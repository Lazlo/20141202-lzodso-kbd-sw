/* twi.c */

#include "twi.h"

#include "config.h"

#include <stdbool.h>

#include <avr/io.h>

struct twi_regs_s {
	volatile uint8_t *twar;		/**< Memory address of TWI slave address register. */
	volatile uint8_t *twbr;		/**< Memory address of TWI bit rate register. */
	volatile uint8_t *twcr;		/**< Memory address of TWI control register. */
	volatile uint8_t *twdr;		/**< Memory address of TWI data register. */
	volatile uint8_t *twsr;		/**< Memory address of TWI status register. */
};

struct twi_dev_s {
	const struct twi_pins_s *pin;	/**< Pointer to structure containing the two pins relevant to the TWI interface.
					 * Only used in slave mode. */
	const struct twi_regs_s *r;	/**< Pointer sturecture with TWI register memory addresses. */
};

static void setslaveaddr(const uint8_t addr);
static void setgcenable(const bool gce);
static void powerup(void);
static uint8_t slavestatus(void);

#ifdef CONFIG_DEBUG_TWI
#include <syslog.h>
#define twdbg syslog
#else
#define twdbg (void)
#endif

static struct twi_regs_s s_twiregs = {
	.twar	= &TWAR,
	.twbr	= &TWBR,
	.twcr	= &TWCR,
	.twdr	= &TWDR,
	.twsr	= &TWSR,
};

static struct twi_dev_s s_twidev = {
	.r	= &s_twiregs,
};

static void setslaveaddr(const uint8_t addr)
{
	twdbg("twi: TWAR\r\n");
#if 0
	enum { SLA_MASK_7BIT = ((1 << TWA6)|(1 << TWA5)|(1 << TWA4)|(1 << TWA3)|
                                (1 << TWA2)|(1 << TWA1)|(1 << TWA0))
	};
	enum { TWAR_TWA_SHIFT = 1 };
	uint8_t regval = 0;

	regval = addr & SLA_MASK_7BIT;
	regval <<= TWAR_SLA_SHIFT;
	if (*s_twidev->r->twar & (1 << TWGCE))
		regval |= (1 << TWGCE);
#else
	volatile uint8_t *reg = s_twidev.r->twar;
	uint8_t regval = addr << 1;
	*reg = regval;
#endif
}

static void setgcenable(const bool gce)
{
	volatile uint8_t *reg = s_twidev.r->twar;
	const uint8_t mask = (1 << 0);

	if (gce)
		*reg |= mask;
	else
		*reg &= ~mask;
}

static void powerup(void)
{
	volatile uint8_t *reg = s_twidev.r->twcr;
	const uint8_t mask = (1 << TWSTA)|(1 << TWEN);
	twdbg("twi: TWCR\r\n");
	*reg |= mask;
}

static uint8_t slavestatus(void)
{
	enum {
		TWSR_TWPS_MASK = ((1 << TWPS1)|(1 << TWPS0))
	};
	volatile uint8_t *reg = s_twidev.r->twsr;
	uint8_t status;
	status = *reg & TWSR_TWPS_MASK;

	return status;
}

void twi_slaveinit(const uint8_t addr, const struct twi_pins_s *pins)
{
	s_twidev.pin = pins;

	/* TODO Initialize TWI IO pins */
#if 0
	gpio_setpinconfig(s_twidev.pin.scl, false, true, false);
	gpio_setpinconfig(s_twidev.pin.sda, false, true, false);
#endif

	setslaveaddr(addr);
	setgcenable(true);
	powerup();
}

void twi_slavepoll(void)
{
#if 1
	enum {
		TWSR_TWS_MASK = ((1 << TWS7)|(1 << TWS6)|(1 << TWS5)|
                                 (1 << TWS4)|(1 << TWS3))
	};
	uint8_t status = slavestatus();
	char code[3] = {[2] = '\0'};
	bool event = false;

	/* TODO process status and dispatch */
	switch (status)
	{

	/* TODO Possible slave receiver status codes */

	case 0x60:
		// SW: RX A, TX ACK (60)\r\n");
		code[0] = '6';
		code[1] = '0';
		event = true;
		break;
	case 0x68:
		// SW: RX A, TX ACK (68)\r\n");
		code[0] = '6';
		code[1] = '8';
		event = true;
		break;
	case 0x70:
		// GC: RX A, TX ACK (70)\r\n");
		code[0] = '7';
		code[1] = '0';
		event = true;
		break;
	case 0x78:
		// GC: RX A, TX ACK (78)\r\n");
		code[0] = '7';
		code[1] = '8';
		event = true;
		break;
	case 0x80:
		// SW: RX D, TX ACK (80)\r\n");
		code[0] = '8';
		code[1] = '0';
		event = true;
		break;
	case 0x88:
		// SLA+W: RX D, TX NACK (88)\r\n");
		code[0] = '8';
		code[1] = '8';
		event = true;
		break;
	case 0x90:
		// GC: RX D, TX ACK (90)\r\n");
		code[0] = '9';
		code[1] = '0';
		event = true;
		break;
	case 0x98:
		// GC: RX D, TX NACK (98)\r\n");
		code[0] = '9';
		code[1] = '8';
		event = true;
		break;
	case 0xA0:
		// RX STOP/RESTART (A0)\r\n");
		code[0] = 'A';
		code[1] = '0';
		event = true;
		break;

	/* TODO Possible slave transmitter status codes */


	case 0xF8:
#ifdef CONFIG_DEBUG_TWI_SHOW_NOP
		// ... (F8)\r\n");
		code[0] = 'F';
		code[1] = '8';
		event = true;
#endif
		break;

	case 0x00:
#ifdef CONFIG_DEBUG_TWI_SHOW_BUSERR
		// BUSERR (00)\r\n");
		code[0] = '0';
		code[1] = '0';
		event = true;
#endif
		break;

	default:
		code[0] = '?';
		code[1] = '?';
		event = true;
		break;
	}

	if (event)
	{
		twdbg("twi: ");
		twdbg(code);
		twdbg("\r\n");
	}
#endif
}
