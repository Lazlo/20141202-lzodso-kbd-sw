/* uart.c */

#include "uart.h"

#include <stdbool.h>

struct uart_dev_s {
	volatile uint8_t *brrh;		/**< Memory address of UART baud rate register (higher byte). */
	volatile uint8_t *brrl;		/**< Memory address of UART baud rate register (lower byte). */
	volatile uint8_t *csra;		/**< Memory address of UART control/status register A. */
	volatile uint8_t *csrb;		/**< Memory address of UART control/status register B. */
	volatile uint8_t *csrc;		/**< Memory address of UART control/status register C. */
	volatile uint8_t *dr;		/**< Memory address of UART data register. */
};

static void setbaudrate(const uint32_t baud, const uint32_t cpufreq);
static void setframeformat(void);
static void settrxenable(bool on);
static bool istxready(void);
static void waittxready(void);
static void transmit(const uint8_t byte);
static bool isrxready(void);
static void waitrxready(void);
static uint8_t receive(void);

#include <avr/io.h>

static struct uart_dev_s g_uart0 = {
#if defined(__AVR_ATmega48__)
	.brrh	= &UBRR0H,
	.brrl	= &UBRR0L,
	.csra	= &UCSR0A,
	.csrb	= &UCSR0B,
	.csrc	= &UCSR0C,
	.dr	= &UDR0,
#elif defined(__AVR_ATmega8__)
	.brrh	= &UBRRH,
	.brrl	= &UBRRL,
	.csra	= &UCSRA,
	.csrb	= &UCSRB,
	.csrc	= &UCSRC,
	.dr	= &UDR,
#else
#error "not supported"
#endif
};

static void setbaudrate(const uint32_t baud, const uint32_t cpufreq)
{
	struct uart_dev_s *dev = &g_uart0;
	uint16_t regval = 0;

#if 0
	regval = (cpufreq / (16 * baud)) - 1;
#else
	switch (baud)
	{
	case 9600:	regval = 129;	break; //   9600 @ 20 MHz (tested: okay)
	case 19200:	regval = 64;	break; //  19200 @ 20 MHz (tested: okay)
	case 38400:	regval = 32;	break; //  38400 @ 20 MHz (tested: okay)
	case 57600:	regval = 21;	break; //  57600 @ 20 MHz (tested: okay)
	case 115200:	regval = 10;	break; // 115200 @ 20 MHz (tested: okay)
	// TODO Check why 250000 bps produces garbage output
	case 250000:	regval = 4;	break; // 250000 @ 20 MHz
	}
#endif
	*dev->brrh = (regval >> 8);
	*dev->brrl = regval & 0x00FF;
}

static void setframeformat(void)
{
	// Set frame format (8data implicity by register reset values)
	//UCSR0B |= UCSZ02
	//UCSR0C |= UCSZ01
	//UCSR0C |= UCSZ00;
}

static void settrxenable(bool on)
{
	struct uart_dev_s *dev = &g_uart0;
#if defined(__AVR_ATmega48__)
	const uint8_t mask = (1 << TXEN0)|(1 << RXEN0);
#elif defined(__AVR_ATmega8__)
	const uint8_t mask = (1 << TXEN)|(1 << RXEN);
#else
#error "not supported"
#endif

	if (on)
		*dev->csrb |= mask;
	else
		*dev->csrb &= ~mask;
}

static bool istxready(void)
{
	struct uart_dev_s *dev = &g_uart0;
#if defined(__AVR_ATmega48__)
	const uint8_t mask = (1 << UDRE0);
#elif defined(__AVR_ATmega8__)
	const uint8_t mask = (1 << UDRE);
#else
#error "not supported"
#endif

	if (*dev->csra & mask)
		return true;
	return false;
}

static void waittxready(void)
{
	uint8_t tout = ~0;

	// while data register not empty wait (with timeout)
	while (!istxready() || tout--)
		;
}

static void transmit(const uint8_t byte)
{
	struct uart_dev_s *dev = &g_uart0;

	*dev->dr = byte;
}

static bool isrxready(void)
{
	struct uart_dev_s *dev = &g_uart0;
#if defined(__AVR_ATmega48__)
	const uint8_t mask = (1 << RXC0);
#elif defined(__AVR_ATmega8__)
	const uint8_t mask = (1 << RXC);
#else
#error "not supported"
#endif
	if (*dev->csra & mask)
		return true;
	return false;
}

static void waitrxready(void)
{
	uint8_t tout = ~0;

	// while data not ready wait (with timeout)
	while (!isrxready() || tout--)
		;
}

static uint8_t receive(void)
{
	struct uart_dev_s *dev = &g_uart0;

	return *dev->dr;
}

void uart_init(const uint32_t baud)
{
	setbaudrate(baud, F_CPU);
	setframeformat();
	settrxenable(true);
}

void uart_putc(const char c)
{
	waittxready();
	transmit(c);
}

bool uart_received(void)
{
	return isrxready();
}

char uart_getc(void)
{
	waitrxready();
	return receive();
}
