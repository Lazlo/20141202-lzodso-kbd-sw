/* timer1.c */

#include "config.h"
#include "timer1.h"

#include <stdint.h>

// Timer1 Registers and Bits:
// TCCR1A		COM1A1|COM1A0|COM1B1|COM1B0|-|-|WGM11|WGM10
// TCCR1B		ICNC1|ICSE1|-|WGM13|WGM12|CS12|CS11|CS10
// TCCR1C		FOC1A|FOC1B|-|-|-|-|-|-
// TCNT1H,TCNT1L
// OCR1AH,OCR1AL
// OCR1BH,OCR1BL
// ICR1H,ICR1L
// TIMSK1		-|-|ICIE1|-|-|OCIE1B|OCIE1A|TOIE1
// TIFR1		-|-|ICF1|-|-|OCF1B|OCF1A|TOV1

enum timer1_wgmode_e {
	TIMER1_WGM_NORMAL = 0,
	TIMER1_WGM_PWM_PHASE_CORRECT_8BIT,
	TIMER1_WGM_PWM_PHASE_CORRECT_9BIT,
	TIMER1_WGM_PWM_PHASE_CORRECT_10BIT,
	TIMER1_WGM_CTC_OCR1A,
	TIMER1_WGM_PWM_FAST_8BIT,
	TIMER1_WGM_PWM_FAST_9BIT,
	TIMER1_WGM_PWM_FAST_10BIT,
	TIMER1_WGM_PWM_PHASE_AND_FREQ_CORRECT_ICR1,
	TIMER1_WGM_PWM_PHASE_AND_FREQ_CORRECT_OCR1A,
	TIMER1_WGM_PWM_PHASE_CORRECT_ICR1,
	TIMER1_WGM_PWM_PHASE_CORRECT_OCR1A,
	TIMER1_WGM_CTC_ICR1,
	TIMER1_WGM_RESERVED,
	TIMER1_WGM_PWM_FAST_ICR1,
	TIMER1_WGM_PWM_FAST_OCR1A
};

enum timer1_cs_e {
	TIMER1_CS_NOCLKSRC = 0,
	TIMER1_CS_CLKIO_DIV1,
	TIMER1_CS_CLKIO_DIV8,
	TIMER1_CS_CLKIO_DIV64,
	TIMER1_CS_CLKIO_DIV256,
	TIMER1_CS_CLKIO_DIV1024,
	TIMER1_CS_T1PIN_FALLINGEDGE,
	TIMER1_CS_T1PIN_RISINGEDGE
};

enum timer1_irqsrc_e {
	TIMER1_IRQSRC_INCMPMATCH = 0,
	TIMER1_IRQSRC_OUTCMPMATCH_B,
	TIMER1_IRQSRC_OUTCMPMATCH_A,
	TIMER1_IRQSRC_OVERFLOW
};

struct timer1_cfg {
	enum timer1_wgmode_e	wgmode;	/**< Wave Generation Mode setting. */
	enum timer1_cs_e	clksrc;	/**< Timer clock source. */
	uint16_t		top;	/**< Counter top value to trigger interrupt when match. */
	enum timer1_irqsrc_e	irqsrc;	/**< Interrupt source to use. */
};

struct timer1_dev_s {
	volatile uint8_t *cra;		/**< Memory address of control register A. */
	volatile uint8_t *crb;		/**< Memory address of control register B. */
	volatile uint8_t *crc;		/**< Memory address of control register C. */
	volatile uint8_t *cnth;		/**< Memory address of counter register (higher byte). */
	volatile uint8_t *cntl;		/**< Memory address of counter register (lower byte). */
	volatile uint8_t *ocrah;	/**< Memory address of output compare unit A register (higher byte). */
	volatile uint8_t *ocral;	/**< Memory address of output compare unit A register (lower byte). */
	volatile uint8_t *ocrbh;	/**< Memory address of output compare unit B register (higher byte). */
	volatile uint8_t *ocrbl;	/**< Memory address of output compare unit B register (lower byte). */
	volatile uint8_t *icrh;		/**< Memory address of input capture register (higher byte). */
	volatile uint8_t *icrl;		/**< Memory address of input capture register (lower byte). */
	volatile uint8_t *imsk;		/**< Memory address of interrupt mask register. */
	volatile uint8_t *ifr;		/**< Memory address of interrupt flag register. */
};

static void setwgmode(const struct timer1_dev_s *dev, const enum timer1_wgmode_e mode);
static void setclksrc(const struct timer1_dev_s *dev, const enum timer1_cs_e clksrc);
static void settop(const struct timer1_dev_s *dev, const enum timer1_irqsrc_e irqsrc, const uint16_t top);
static void irqenable(const struct timer1_dev_s *dev, const enum timer1_irqsrc_e irqsrc);

#include <avr/io.h>

static const struct timer1_dev_s g_timer1_dev = {
	.cra	= &TCCR1A,
	.crb	= &TCCR1B,
#if defined(__AVR_ATmega48__)
	.crc	= &TCCR1C,
#endif
	.cnth	= &TCNT1H,
	.cntl	= &TCNT1L,
	.ocrah	= &OCR1AH,
	.ocral	= &OCR1AL,
	.ocrbh	= &OCR1BH,
	.ocrbl	= &OCR1BL,
	.icrh	= &ICR1H,
	.icrl	= &ICR1L,
#if defined(__AVR_ATmega48__)
	.imsk	= &TIMSK1,
	.ifr	= &TIFR1,
#elif defined(__AVR_ATmega8__)
	.imsk	= &TIMSK,
	.ifr	= &TIFR,
#else
#error "not supported"
#endif
};

static const struct timer1_cfg g_timer1_cfg = {
	.wgmode	= TIMER1_WGM_CTC_OCR1A,
	.clksrc	= TIMER1_CS_CLKIO_DIV1, // 1,8,64,256,1024
	.top	= 20000, // 1 ms * (20000000 Hz / 1 / 1000) = 20000
	.irqsrc	= TIMER1_IRQSRC_OUTCMPMATCH_A
};

/* TODO Turn into array of function pointers */
static void (*g_timer1_capt_isr)(void);
static void (*g_timer1_compb_isr)(void);
static void (*g_timer1_compa_isr)(void);
static void (*g_timer1_ovf_isr)(void);

#ifdef CONFIG_DEBUG_TIMER1
#include "syslog.h"
#define t1dbg syslog
#else
#define t1dbg (void)
#endif

static void setwgmode(const struct timer1_dev_s *dev, const enum timer1_wgmode_e wgmode)
{
	/* First, clear the WGM bits in both registers */
	/* Then set waveform generation mode */

	*dev->crb &= ~((1 << WGM13)|(1 << WGM12));
	*dev->cra &= ~((1 << WGM11)|(1 << WGM10));
	t1dbg("timer1: WGM: ");
	if (wgmode & (1 << 3))	{ *dev->crb |= (1 << WGM13); t1dbg("WGM13"); }
	if (wgmode & (1 << 2))	{ *dev->crb |= (1 << WGM12); t1dbg("WGM12"); }
	if (wgmode & (1 << 1))	{ *dev->cra |= (1 << WGM11); t1dbg("WGM11"); }
	if (wgmode & (1 << 0))	{ *dev->cra |= (1 << WGM10); t1dbg("WGM10"); }
	t1dbg("\r\n");
}

static void setclksrc(const struct timer1_dev_s *dev, const enum timer1_cs_e clksrc)
{
	/* First, clear the clock source bits */
	/* Then set the clock source */

	*dev->crb &= ~((1 << CS12)|(1 << CS11)|(1 << CS10));
	t1dbg("timer1: CS: ");
	if (clksrc & (1 << 2))	{ *dev->crb |= (1 << CS12); t1dbg(" CS12"); }
	if (clksrc & (1 << 1))	{ *dev->crb |= (1 << CS11); t1dbg(" CS11"); }
	if (clksrc & (1 << 0))	{ *dev->crb |= (1 << CS10); t1dbg(" CS10"); }
	t1dbg("\r\n");
}

static void settop(const struct timer1_dev_s *dev, const enum timer1_irqsrc_e irqsrc, const uint16_t top)
{
	uint8_t h = (top >> 8) & 0xFF;
	uint8_t l = top & 0xFF;

	/* Set counter top value */
	t1dbg("timer1: TOP: ");
	switch(irqsrc)
	{
	case TIMER1_IRQSRC_INCMPMATCH:
		t1dbg("ICR");
		*dev->icrh = h;
		*dev->icrl = l;
		break;
	case TIMER1_IRQSRC_OUTCMPMATCH_B:
		t1dbg("OCRB");
		*dev->ocrbh = h;
		*dev->ocrbl = l;
		break;
	case TIMER1_IRQSRC_OUTCMPMATCH_A:
		t1dbg("OCRA");
		*dev->ocrah = h;
		*dev->ocral = l;
		break;
	case TIMER1_IRQSRC_OVERFLOW:
		t1dbg("CNT");
		/* TODO Check if that is the correct register for or this makes sense at all */
		*dev->cnth = h;
		*dev->cntl = l;
		break;
	}
	t1dbg("\r\n");
}

static void irqenable(const struct timer1_dev_s *dev, const enum timer1_irqsrc_e irqsrc)
{
	volatile uint8_t *r = dev->imsk;
	t1dbg("timer1: IRQ: ");
	switch (irqsrc)
	{
#if defined(__AVR_ATmega48__)
	case TIMER1_IRQSRC_INCMPMATCH:		*r |= (1 << ICIE1); t1dbg("ICIE"); break;
#elif defined(__AVR_ATmega8__)
	case TIMER1_IRQSRC_INCMPMATCH:		*r |= (1 << TICIE1); t1dbg("ICIE"); break;
#else
#error "not supported"
#endif
	case TIMER1_IRQSRC_OUTCMPMATCH_B:	*r |= (1 << OCIE1B); t1dbg("OCIE1B"); break;
	case TIMER1_IRQSRC_OUTCMPMATCH_A:	*r |= (1 << OCIE1A); t1dbg("OCIE1A");  break;
	case TIMER1_IRQSRC_OVERFLOW:		*r |= (1 << TOIE1); t1dbg("OVF"); break;
	}
	t1dbg("\r\n");
}

void timer1_init(void)
{
	const struct timer1_dev_s	*dev	= &g_timer1_dev;
	const struct timer1_cfg		*cfg	= &g_timer1_cfg;

	enum timer1_cs_e		clksrc	= cfg->clksrc;
	enum timer1_wgmode_e		wgmode	= cfg->wgmode;
	enum timer1_irqsrc_e		irqsrc	= cfg->irqsrc;
	uint16_t			top	= cfg->top;

	setwgmode(dev, wgmode);
	settop(dev, irqsrc, top);
	irqenable(dev, irqsrc);
	/* Enable timer by selecting clock source and prescaler (other than 0) */
	setclksrc(dev, clksrc);
}

void timer1_sethandler(void (*handler)(void))
{

	const enum timer1_irqsrc_e irqsrc = g_timer1_cfg.irqsrc; /* TODO HACK! */

	t1dbg("timer1: ISR: ");
	switch (irqsrc)
	{
	case TIMER1_IRQSRC_INCMPMATCH:
		t1dbg("CAPT");
		g_timer1_capt_isr = handler;
		break;
	case TIMER1_IRQSRC_OUTCMPMATCH_B:
		t1dbg("COMPB");
		g_timer1_compb_isr = handler;
		break;
	case TIMER1_IRQSRC_OUTCMPMATCH_A:
		t1dbg("COMPA");
		g_timer1_compa_isr = handler;
		break;
	case TIMER1_IRQSRC_OVERFLOW:
		t1dbg("OVF");
		g_timer1_ovf_isr = handler;
		break;
	}
	t1dbg("_vect\r\n");
}

#include <avr/interrupt.h>

ISR(TIMER1_CAPT_vect)	{ (*g_timer1_capt_isr)(); }
ISR(TIMER1_COMPB_vect)	{ (*g_timer1_compb_isr)(); }
ISR(TIMER1_COMPA_vect)	{ (*g_timer1_compa_isr)(); }
ISR(TIMER1_OVF_vect)	{ (*g_timer1_ovf_isr)(); }
