/* vuart.c */

#include "vuart.h"

#include "config.h"

#ifndef CONFIG_SIMULAVR_OUT_REG
#error "CONFIG_SIMULAVR_OUT_REG is not defined!"
#endif
#ifndef CONFIG_SIMULAVR_IN_REG
#error "CONFIG_SIMULAVR_IN_REG is not defined"
#endif

#define vuart_reg_out	(*((volatile char *)CONFIG_SIMULAVR_OUT_REG))
#define vuart_reg_in	(*((volatile char *)CONFIG_SIMULAVR_IN_REG))

void vuart_putc(const char c)
{
	vuart_reg_out = c;
}

char vuart_getc(void)
{
	return vuart_reg_in;
}
