/* main.c */

/** \mainpage Firmware Documentation
 *
 * \section abstract_sec Abstract
 *
 * The firmware for the keypad will scan the <b>button matrix</b>
 * and <b>rotary encoders</b> periodically and make the detected changes
 * availble using a <b>bus interface</b> to be further processed by
 * another system.
 *
 * The code that implements this behaviour can be separated into
 * the following logical layers:
 *
 *  - configuration
 *  - application
 *  - operation system
 *  - peripherals drivers
 *
 * \subsection config_sec Configuration
 *
 * See include/config.h
 *
 * \subsection app_sec Application
 *
 * For a lack of a better name, we called the application <b>netkbd</b>.
 * It ties togeter the keyboard and the bus interface.
 *
 * The keyboard code is separated into a generic keyboard API (see include/kbd.h) and
 * a module that is specific the the keyboard hardware of the lzoDSO (see include/ldk.h).
 *
 * \subsection os_sec Operating System
 *
 * Main responsibility of the operating system is to provide a mechanism
 * for execution of multiple tasks. This is implemented using a <b>co-operative
 * scheduler</b> (see include/sys/sched.h).
 *
 * Aside from task scheduling, the OS tries to provide platform-independend
 * services for common features such as a <b>system tick</b>, <b>uptime</b>,
 * <b>console</b>, <b>logging facility</b> and <b>bus interface</b>.
 *
 * \subsection drv_sec Peripherals Drivers
 *
 * - GPIO
 * - Interrupt Subsystem
 * - LED
 * - parallel-in / serial-out shift register
 * - serial-in / parallel-out shift register
 * - SPI
 * - Timer
 * - I2C
 * - UART
 */

#include "config.h"
#include "hw/gpio.h"
#include "hw/led.h"
#ifndef CONFIG_NO_TWI
#include "hw/twi.h"
#endif
#ifndef CONFIG_NO_SPI
#include "hw/spi.h"
#endif
#include "sys/busif.h"
#include "sys/syslog.h"
#include "sys/sched.h"
#include "ldk.h"
#include "kbd.h"
#include "os.h"

/* GPIO Pins Used */

enum sys_gpio_e {
	GPIO_USART0_RXD = 0,
	GPIO_USART0_TXD,
	GPIO_LED1,
	GPIO_SIPO_DATAOUT,
	GPIO_SIPO_CLOCK,
	GPIO_SIPO_CLEAR,
	GPIO_SPI_SCK,
	GPIO_SPI_MISO,
	GPIO_SPI_MOSI,
	GPIO_SPI_SS,
	GPIO_PISO_DATAIN,
	GPIO_PISO_CLOCK,
	GPIO_PISO_LATCH,
	GPIO_BUSIF_INTOUT,
	GPIO_TWI_SCL,
	GPIO_TWI_SDA,
};

/* System GPIOs */

#include <avr/io.h>

#define PIN(port, num)	{	.nr		= num,			\
				.reg_dir	= &DDR##port,		\
				.reg_in		= &PIN##port,		\
				.reg_out	= &PORT##port }

static struct pin_s g_sys_gpio[] = {
				/* USART Pins (fixed)		*/
	[GPIO_USART0_RXD]	= PIN(D, 0),	/* PD0 */
	[GPIO_USART0_TXD]	= PIN(D, 1),	/* PD1 */
				/* LEDs (used for debugging)	*/
	[GPIO_LED1]		= PIN(D, 3),	/* PD3 */
				/* SIPO Shift Register Pins	*/
	[GPIO_SIPO_DATAOUT]	= PIN(D, 7),	/* PD7 */
	[GPIO_SIPO_CLOCK]	= PIN(D, 5),	/* PD5 */
	[GPIO_SIPO_CLEAR]	= PIN(D, 6),	/* PD6 */
				/* SPI Pins (fixed)		*/
	[GPIO_SPI_SCK]		= PIN(B, 5),	/* PB5 */
	[GPIO_SPI_MISO]		= PIN(B, 4),	/* PB4 */
	[GPIO_SPI_MOSI]		= PIN(B, 3),	/* PB3 */
	[GPIO_SPI_SS]		= PIN(B, 2),	/* PB2 */
				/* PISO Shift Register Pins	*/
	[GPIO_PISO_DATAIN]	= PIN(C, 2),	/* PC2 */
	[GPIO_PISO_CLOCK]	= PIN(C, 0),	/* PC0 */
	[GPIO_PISO_LATCH]	= PIN(C, 1),	/* PC1 */
				/* Bus Interface Interrupt Pin	*/
	[GPIO_BUSIF_INTOUT]	= PIN(C, 3),	/* PC3 */
				/* TWI Pins (fixed)		*/
	[GPIO_TWI_SCL]		= PIN(C, 5),	/* PC5 */
	[GPIO_TWI_SDA]		= PIN(C, 4),	/* PC4 */
};

/* lzoDSO keyboard IO operations */

#include "hw/sipo.h"
#include "hw/piso.h"

const static struct ldk_ioop_s g_ldk_ioop = {
	.fp_sipo_reset		= sipo_reset,
	.fp_sipo_write		= sipo_write,
	.fp_sipo_clock		= sipo_clock,
	.fp_piso_latch		= piso_latch,
	.fp_piso_readbyte	= piso_readbyte,
};

#ifndef CONFIG_NO_TWI
const static struct twi_pins_s g_twipins = {
	.scl		= &g_sys_gpio[GPIO_TWI_SCL],
	.sda		= &g_sys_gpio[GPIO_TWI_SDA],
};
#endif

#ifndef CONFIG_NO_SPI
/* SPI slave mode */

const static struct spi_cfg_s g_spicfg = {
	.master		= false,
	.sckmode	= 0,
	.sckrate	= 0,	/* ingored for slave, set 0 anyway */
	.lsbfirst	= false,
};

const static struct spi_pins_s g_spipins = {
	.sck		= &g_sys_gpio[GPIO_SPI_SCK],
	.miso		= &g_sys_gpio[GPIO_SPI_MISO],
	.mosi		= &g_sys_gpio[GPIO_SPI_MOSI],
	.ss		= &g_sys_gpio[GPIO_SPI_SS],
};
#endif

/* Bus Interface IO operations */

static struct busif_io_s g_busif_io = {
	.update		= spi_update,
	.rxready	= spi_rxready,
	.read		= spi_read,
	.txready	= spi_txready,
	.write		= spi_write,
};

static void initkbd(void);
static void initbusif(void);
static void netkbd_init(void);
static void netkbd_update(void);
static void initboard(void);
static void addtasks(void);

#ifndef CONFIG_NO_LOGREADY
#define log_ready(s)	syslog(s " ready\r\n")
#else
#define log_ready(s)
#endif

static void initkbd(void)
{
	/* Initialize Shift Registers */
	sipo_init(&g_sys_gpio[GPIO_SIPO_DATAOUT],
			&g_sys_gpio[GPIO_SIPO_CLOCK],
			&g_sys_gpio[GPIO_SIPO_CLEAR]);	/* Setup 74x164 pins */
	piso_init(&g_sys_gpio[GPIO_PISO_DATAIN],
			&g_sys_gpio[GPIO_PISO_CLOCK],
			&g_sys_gpio[GPIO_PISO_LATCH]);	/* Setup 74x165 pins */
	/* Setup the keyboard specific logic */
	ldk_init(&g_ldk_ioop, kbd_enqueue_key);
	/* Setup the generic keyboard logic */
	kbd_init(ldk_scan, ldk_changed);
	log_ready("kbd");
}

static void initbusif(void)
{
#ifndef CONFIG_NO_TWI
#ifndef CONFIG_SIMULAVR
	/* Setup the TWI module as slave */
	twi_slaveinit(CONFIG_TWIADDR, &g_twipins);
	log_ready("twi");
#endif
#endif
#ifndef CONFIG_NO_SPI
	/* Setup the SPI modue as slave */
	spi_init(&g_spicfg, &g_spipins);
	log_ready("spi");
#endif
	/* Setup the bus interface abstraction */
	busif_init(&g_busif_io, &g_sys_gpio[GPIO_BUSIF_INTOUT], g_spicfg.master);
	log_ready("busif");
}

static void netkbd_init(void)
{
	initkbd();
	initbusif();
}

#ifdef CONFIG_DEBUG_NETKBD
#include "syslog.h"
#include <stdlib.h> /* for itoa() */
#define nkdbg syslog
#else
#define nkdbg (void)
#endif

static void netkbd_update(void)
{
	uint8_t changed;

	/* TODO Check if there was a request from the bus interface */

	/* Check if there are keys that changed */
	if ((changed = kbd_changed()) == 0)
		return;

	uint8_t key;
#ifdef CONFIG_DEBUG_NETKBD
	char keystr[4];
#endif

#ifdef CONFIG_DEBUG_NETKBD
	nkdbg("n c ");
	itoa(changed, keystr, 10);
	nkdbg(keystr);
	nkdbg(" (");
#endif

	uint8_t i;
	for (i = 0; i < changed; i++) {
		kbd_getkey(&key);

#ifdef CONFIG_DEBUG_NETKBD
		itoa(key, keystr, 10);
		nkdbg(keystr);
		if (i+1 < changed)
			nkdbg(" ");
#endif
		/* Enqueue the data in the bus interface (and let it notify
		 * its bus master (call busif_write()) */
		busif_put(key);
	}
#ifdef CONFIG_DEBUG_NETKBD
	nkdbg(")");
	nkdbg("\r\n");
#endif
}

static void initboard(void)
{
	/* Initialize LED */
	ledinit(&g_sys_gpio[GPIO_LED1]);

	netkbd_init();
}

static void addtasks(void)
{
	sched_addtask(ledtoggle,	0, 1000);
	sched_addtask(kbd_update,	0,   50);
	sched_addtask(busif_update,	0,   50);
	sched_addtask(netkbd_update,	0,   50);
}

int main(void)
{
	os_init();
	/* Initialize board support */
	initboard();
	/* Setup tasks */
	addtasks();
	ledset(true);
	os_start();
}
