#pragma once

/* kbd.h */

#include <stdbool.h>
#include <stdint.h>

void kbd_init(void (*fp_scan)(void), uint8_t (*fp_changed)(void));

void kbd_update(void);

uint8_t kbd_changed(void);

void kbd_getkey(uint8_t *key);

/* Part of the internal API between KBD and low-level backends (such as LDK) */

void kbd_enqueue_key(const uint8_t key);
