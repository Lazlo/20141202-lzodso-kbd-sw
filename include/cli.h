#pragma once

/* cli.h */

void cli_init(void (*putc)(char c), char (*getc)(void));

void cli_update(void);
