#pragma once

/* ldk.h */

#include "gpio.h"

struct ldk_ioop_s {
	void (*fp_sipo_reset)(void);
	void (*fp_sipo_write)(const bool);
	void (*fp_sipo_clock)(void);

	void (*fp_piso_latch)(void);
	uint8_t (*fp_piso_readbyte)(void);
};

void ldk_init(const struct ldk_ioop_s *io, void (*enqueue_key)(const uint8_t));

void ldk_scan(void);

uint8_t ldk_changed(void);
