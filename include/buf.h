#pragma once

/* buf.h */

#include <stdint.h>

void clearbuf(uint8_t *buf, const uint8_t len);
void cpybuf(uint8_t *to, const uint8_t *from, const uint8_t len);
void movebytes(uint8_t *to, uint8_t *from, const uint8_t len);
void dumpbuf(const uint8_t *buf, const uint8_t len, void (*fp_puts)(char *));
