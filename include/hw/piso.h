#pragma once

/* piso.h */

#include "gpio.h"

void piso_init(struct pin_s *pin_datain,
		struct pin_s *pin_clock,
		struct pin_s *pin_latch);

void piso_latch(void);
void piso_clock(void);
bool piso_read(void);
uint8_t piso_readbyte(void);
