#pragma once

/* spi.h */

#include <stdbool.h>
#include <stdint.h>

#include "gpio.h"

struct spi_pins_s {
	struct pin_s *sck;	/**< SPI clock pin. */
	struct pin_s *miso;	/**< master-in/slave-out pin. */
	struct pin_s *mosi;	/**< master-out/slave-in pin. */
	struct pin_s *ss;	/**< chip select pin. */
};

struct spi_cfg_s {
	bool master;		/**< Set to true when to be used as master, or false when to be used as slave. */
	uint8_t sckmode;	/**< Clock mode settings (phase and idle state). */
	uint8_t sckrate;	/**< Clock speed setting. */
	bool lsbfirst;		/**< Bit order configuration. */
};

void spi_init(const struct spi_cfg_s *c, const struct spi_pins_s *pins);

void spi_update(void);

/* Bus Interface API */

bool spi_rxready(void);

uint8_t spi_read(void);

bool spi_txready(void);

void spi_write(const uint8_t b);
