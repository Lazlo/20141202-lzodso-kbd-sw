#pragma once

/** \file gpio.h
 * \brief Control of General purpose input/output (GPIO) pin driver.
 */

#include <stdbool.h>
#include <stdint.h>

/** \brief General purpose input/output (GPIO) pin data structure.
 *
 * The motivation for having this structure is to allow operations
 * to be performed on a GPIO using the functions of this module without
 * the caller having to know the hardware related details like addresses of registers.
 * This way, our GPIO function can do their job by looking up the respective
 * registers to work on by looking them up in instances of this structure.
 */
struct pin_s {
	uint8_t nr;			/**< Offset of the pin within the bank (starts at 0). */
	volatile uint8_t *reg_dir;	/**< Memory address of GPIO direction control register. */
	volatile uint8_t *reg_in;	/**< Memory address of GPIO data input/pull-up control register. */
	volatile uint8_t *reg_out;	/**< Memory address of GPIO data output register. */
};

/** \brief Custom type represending a pin.
 *
 * This type is used as a context given to the GPIO functions.
 */
typedef struct pin_s pin_t;

/** \brief Initialize a GPIO object.
 *
 * Will write the pin offset and register addresses into the pin data structure.
 * It will perform any read or write operation on these registers.
 */
void gpio_create(pin_t *pin, const uint8_t nr,
			volatile uint8_t *reg_dir,
			volatile uint8_t *reg_in,
			volatile uint8_t *reg_out);

/** \brief Read pin configuration.
 *
 * \note Not implemented.
 */
void gpio_getpinconfig(pin_t *pin);

/** \brief Controls pin direction.
 *
 * \note Control of pull-up/pull-down resistors is not implemented.
 */
void gpio_setpinconfig(pin_t *pin, bool output, bool pullup, bool pulldown);

/** \brief Controls the pin output signal.
 *
 * Will set the output to high when on is true. Will set output to low when on is false.
 */
void gpio_writepin(pin_t *pin, bool on);

/** \brief Read pin input signal.
 *
 * Returns the true if the pin input signal is high, returns false otherwise (pin input signal is low).
 */
bool gpio_readpin(pin_t *pin);
