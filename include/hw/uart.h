#pragma once

/** \file uart.h
 * \brief Universal Asynchronous Receiver/Transmitter driver.
 */

#include <stdint.h>
#include <stdbool.h>

/** \brief Initialize UART hardware.
 *
 * Will set the baud rate, frame format (to 8-bits) and enable
 * transmission and reception circuitry.
 */
void uart_init(const uint32_t baud);

/** \brief Have the UART transmit a byte.
 *
 * As this function will wait until the UART is ready for transmission
 * this function will block if the UART is not ready.
 */
void uart_putc(const char c);

/** \brief Check if the UART received a byte.
 *
 * Will never block as it reads a status bit from the UART hardware.
 */
bool uart_received(void);

/** \brief Wait for and return a byte receive by the UART.
 *
 * As this function will wait for a byte being received it will block
 * if not byte has been recevied.
 */
char uart_getc(void);
