#pragma once

/* pwr.h */

#include <stdbool.h>

void pwr_sleep(void);

void pwr_sleep_enable(const bool on);
