#pragma once

/* twi.h */

#include <stdint.h>

#include "gpio.h"

struct twi_pins_s {
	struct pin_s *scl;	/**< Serial clock pin */
	struct pin_s *sda;	/**< Serial data pin */
};

void twi_slaveinit(const uint8_t addr, const struct twi_pins_s *pins);

void twi_slavepoll(void);
