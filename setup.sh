#!/bin/bash

pkgs=""
pkgs="$pkgs make"
pkgs="$pkgs gcc-avr"
pkgs="$pkgs avr-libc"
pkgs="$pkgs avrdude"
pkgs="$pkgs cloc xsltproc"
pkgs="$pkgs doxygen"

apt-get update && apt-get -q install -y $pkgs
