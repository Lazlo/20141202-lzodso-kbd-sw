== bugs ==
 * rotary encoder changes (sometimes) when key is pressed
 * timer interrupt frequency is not correct?
== features ==
=== startup ===
 * initialize queue/lifo
 * schedule kbd controller task (has register interface towards bus if)
 * schedule kbd input processing
 * debounce keys
 * decode direction from encoder
 * keyboard register interface
=== Console ===
 * implement reading keys (so we can use them in simulavr to simulate key changes)
=== SPI ===
 * ~~remove TX/RX buffer from source module (should be handled by busif)~~
=== build system ===
 * make CONFIG_SIMULAVR be defined on call of sim targets (using -DCONFIG_SIMULAVR)
 * setup SPI output for simulavr
 * add target for avr-nm --size-sort
